# Printing Services

Printing Services software bundle for small time businesses.

## TODO
- [ ] Manage printers list
- [ ] and print queues
- [ ] Allows users to use templates for printing 
    - [ ] Photos
    - [ ] ID pictures (eg 1x1)
    - [ ] Creates printing packages for photos
    - [ ] Powerpoint quick print ( define how many slides per page )
- [ ] Handles outside printing sources
    - [ ] Messenger Uploads
    - [ ] Email
    - [ ] Flash Drives
- [ ] Remote printing
    - [ ] Allow other known users to just drop off their files from a convenient webpage, and pickup the hard copies on site.
    - [ ] ... features?
- [ ] Handle pricing scheme

- [ ] Create CI definitions for automating builds for electron and vue


### Tools?

- [Electron](https://electronjs.org/)
- [electron-print](https://www.npmjs.com/package/electron-print)
- VueJS